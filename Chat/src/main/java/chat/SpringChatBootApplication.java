package chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringChatBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringChatBootApplication.class, args);
    }
}