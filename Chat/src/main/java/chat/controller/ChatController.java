package chat.controller;

import chat.message.clientMessage.ClientMessage;
import chat.message.clientMessage.ClientRequest;
import chat.message.serverMessage.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.*;

@Controller
public class ChatController {
    private SimpMessagingTemplate directMessager;
    private Deque<ClientMessage> lastMessages;
    private List<String > onlineUsers;

    private static final int LAST_MESSAGES_COUNT = 10;
    private static int sessionId = 0;

    @Autowired
    public ChatController(SimpMessagingTemplate directMessager) {
        this.directMessager = directMessager;
        this.lastMessages = new ArrayDeque<>();
        this.onlineUsers = new ArrayList<>();
    }

    @MessageMapping("/sendMessage")
    @SendTo("/broker/events/newMessageEvent")
    public NewMessageEvent sendMessage(@Payload ClientMessage clientMessage){
        String userName = clientMessage.getUserName();
        System.out.println("New message[" + clientMessage.getPayload() + "] from {" + userName + "}");
        directMessager.convertAndSend("/broker/responses/messageResponse/" + userName, new ServerResponse(ServerMessage.MessageType.messageResponse, getSessionId()));
        addMessage(clientMessage);
        return new NewMessageEvent(clientMessage);

    }

    @MessageMapping("/register")
    @SendTo("/broker/events/newUserEvent")
    public IOUserEvent addUser(@Payload ClientRequest request, SimpMessageHeaderAccessor headerAccessor){
        String userName = request.getUserName();
        headerAccessor.getSessionAttributes().put("username", userName);

        sendStartInfo(userName);
        onlineUsers.add(userName);
        return new IOUserEvent(ServerMessage.MessageType.userLoginEvent, userName);
    }

    public void deleteUser(String userName){
        onlineUsers.remove(userName);
    }

    private void sendStartInfo (String userName){
        directMessager.convertAndSend("/broker/responses/newUserEvent/" + userName, new ServerResponse(ServerMessage.MessageType.loginResponse, getSessionId()));
        directMessager.convertAndSend("/broker/responses/lastUserMessages/" + userName, new LastUserMessages(new ArrayList<>(lastMessages)));
        directMessager.convertAndSend("/broker/responses/onlineUsers/" + userName, new ServerUserListResponse(onlineUsers));
    }

    private void addMessage(ClientMessage clientMessage){
        if(lastMessages.size() == LAST_MESSAGES_COUNT) {
            lastMessages.pop();
        }
        lastMessages.add(clientMessage);
    }

    private int getSessionId(){
        return ++sessionId;
    }

}
