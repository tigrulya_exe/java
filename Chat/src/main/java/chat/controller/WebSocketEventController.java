package chat.controller;

import chat.message.serverMessage.IOUserEvent;
import chat.message.serverMessage.ServerMessage;
import chat.message.serverMessage.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class WebSocketEventController {

    @Autowired
    ChatController chatController;

    @Autowired
    private SimpMessageSendingOperations template;

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        String userName = (String) headerAccessor.getSessionAttributes().get("username");
        chatController.deleteUser(userName);

        template.convertAndSend("/broker/responses/userLeftEvent" + userName, new ServerResponse(ServerMessage.MessageType.logoutResponse, 0));
        template.convertAndSend("/broker/events/userLogoutEvent", new IOUserEvent(ServerMessage.MessageType.userLogoutEvent, userName));
    }
}