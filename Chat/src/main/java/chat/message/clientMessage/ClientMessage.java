package chat.message.clientMessage;


public class ClientMessage {
    private String userName;
    private String payload;
    private int sessionId;

    public ClientMessage(){}

    public ClientMessage(String userName, String payload, int sessionId) {
        this.userName = userName;
        this.payload = payload;
        this.sessionId = sessionId;
    }

    public String getUserName() {
        return userName;
    }

    public String getPayload() {
        return payload;
    }

    public int getSessionId() {
        return sessionId;
    }
}
