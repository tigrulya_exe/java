package chat.message.clientMessage;

public class ClientRequest {
    private String userName;
    private int sessionId;

    public String getUserName() {
        return userName;
    }

    public int getSessionId() {
        return sessionId;
    }
}
