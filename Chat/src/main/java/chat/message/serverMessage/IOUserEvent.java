package chat.message.serverMessage;

public class IOUserEvent extends ServerMessage {
    private String userName;

    public IOUserEvent(){
    };

    public IOUserEvent(MessageType messageType, String userName) {
        super(messageType);
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
