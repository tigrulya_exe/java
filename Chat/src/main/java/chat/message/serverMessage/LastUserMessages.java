package chat.message.serverMessage;


import chat.message.clientMessage.ClientMessage;

import java.util.List;

public class LastUserMessages extends ServerMessage {
    private List<ClientMessage> clientMessages;

    public LastUserMessages(){}

    public LastUserMessages(List<ClientMessage> clientMessages) {
        super(MessageType.lastMessages);
        this.clientMessages = clientMessages;
    }

    public List<ClientMessage> getClientMessages() {
        return clientMessages;
    }

    public void setClientMessages(List<ClientMessage> clientMessages) {
        this.clientMessages = clientMessages;
    }
}
