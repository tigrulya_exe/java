package chat.message.serverMessage;

import chat.message.clientMessage.ClientMessage;

public class NewMessageEvent extends ServerMessage{
    private String userName;
    private String payload;

    public NewMessageEvent(){}

    public NewMessageEvent(ClientMessage clientMessage){
        super(MessageType.newMessageEvent);
        this.userName = clientMessage.getUserName();
        this.payload = clientMessage.getPayload();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
