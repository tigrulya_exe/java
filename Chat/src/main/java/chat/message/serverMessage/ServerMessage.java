package chat.message.serverMessage;

import org.apache.catalina.Server;

public class ServerMessage {

    private MessageType messageType;

    public enum MessageType {
        nullType,
        message,
        startInfoResponse,
        userLoginEvent,
        userLogoutEvent,
        messageResponse,
        loginResponse,
        logoutResponse,
        newMessageEvent,
        userListResponse,
        lastMessages
    }


    public ServerMessage(MessageType messageType) {
        this.messageType = messageType;
    }

    public ServerMessage(){
        this.messageType = MessageType.nullType;
    }
}
