package chat.message.serverMessage;

public class ServerResponse extends ServerMessage {
    private boolean isSuccess;
    private String errorMessage;
    private int sessionId;

    public ServerResponse(){}

    public ServerResponse(MessageType messageType, int sessionId) {
        super(messageType);
        this.isSuccess = true;
        this.sessionId = sessionId;
        this.errorMessage = null;
    }

    public void setErrorMessage(String errorMessage ){
        this.errorMessage = errorMessage;
        isSuccess = false;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}
