package chat.message.serverMessage;

import java.util.List;

public class ServerUserListResponse {
    List<String> users;

    public ServerUserListResponse(){}

    public ServerUserListResponse(List<String> users) {
        this.users = users;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }
}
