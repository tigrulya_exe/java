var textToSend = document.getElementById("textField");
var stompClient = null;
var chatBox = document.getElementById('chatBox');
var name;
var sessionId = 0;

var userName = document.getElementById('userNameField');

textToSend.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        if(textToSend.value[textToSend.value.length -1 ] === '\n') {
            textToSend.value = textToSend.value.slice(0 , -1);
        }
        document.getElementById("sendButton").click();
    }
});

document.getElementById('userNameField').addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        userName.value = userName.value.slice(0,-1);
        document.getElementById("loginButton").click();
    }
});

function addUserName(newUserName) {
    var newUserNameBox = document.createElement('th');
    newUserNameBox.appendChild(document.createTextNode(newUserName));
    var row = document.createElement('tr');
    row.appendChild(newUserNameBox);
    document.getElementsByTagName('tbody')[0].appendChild(row);
}

function showIOUserEvent(userName, textToShow) {
    var userEventMessage = document.createElement('p');

    userEventMessage.style.color = 'red';
    userEventMessage.style.textAlign = 'center';

    userEventMessage.appendChild(document.createTextNode(userName + textToShow) );
    chatBox.appendChild(userEventMessage);
}

function getNameIndex(table, userName) {
    for(var i = 1; i < table.childNodes.length; ++i){
        if(table.childNodes[i].innerHTML === '<th>' + userName + '</th>'){
            return i;
        }
    }

    return -1;
}

function deleteUserName(userName) {
    var table = document.getElementsByTagName('tbody')[0];
    var nameIndex = getNameIndex(table, userName);
    table.childNodes[nameIndex].remove();
}

function userLoginEvent(event) {
    addUserName(event.userName);
    showIOUserEvent(event.userName, ' joined!');
}

function userLogoutEvent(event) {
    deleteUserName(event.userName);
    showIOUserEvent(event.userName, ' left!');
}

function showMessage(message) {
    var userMessage = document.createElement('p');
    var coloredUserName = document.createElement('span');
    coloredUserName.appendChild(document.createTextNode(message.userName));
    coloredUserName.style.color = 'blue';

    userMessage.appendChild(coloredUserName);
    userMessage.appendChild(document.createTextNode( ' : ' + message.payload));

    chatBox.appendChild(userMessage);
    chatBox.scrollTop = 9999;
}

function handleLoginResponse(response){
    sessionId = response.sessionId;
    console.log(response.success);
}

function showLastMessages(messages) {
    messages.clientMessages.forEach(showMessage);
}

function showOnlineUsers(users) {
    users.users.forEach(addUserName);
}

function handleMessageResponse(messageResponse) {
    console.log("Message response: " + messageResponse.success);
}

function registerHandlers() {
    stompClient.subscribe("/broker/events/newUserEvent", function (event) {
        userLoginEvent(JSON.parse(event.body) );
    });

    stompClient.subscribe("/broker/events/userLogoutEvent", function (event) {
        userLogoutEvent(JSON.parse(event.body) );
    });

    stompClient.subscribe("/broker/events/newMessageEvent", function (message) {
        showMessage(JSON.parse( message.body));
    });

    stompClient.subscribe("/broker/responses/newUserEvent/" + name, function (response) {
        handleLoginResponse(JSON.parse(response.body));
    });

    stompClient.subscribe("/broker/responses/lastUserMessages/" + name, function (response) {
        showLastMessages(JSON.parse(response.body));
    });

    stompClient.subscribe("/broker/responses/onlineUsers/" + name, function (response) {
        showOnlineUsers(JSON.parse(response.body));
    });

    stompClient.subscribe("/broker/responses/messageResponse/" + name, function (response) {
        handleMessageResponse(JSON.parse(response.body));
    });

    stompClient.send("/app/register", {}, JSON.stringify({'userName': name, 'sessionId': sessionId}));
}

function isEmpty(string){
    return string === '' || string === '\n';
}

function connect() {
    name = document.getElementById('userNameField').value;

    if(isEmpty(name)){
        name = '';
        alert("Wrong username!");
        return;
    }

    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        console.log('Connected: ' + frame);
        registerHandlers();
    },
        function (frame) {
            alert("Can't connect to server :(");
            document.getElementById('reg').style.display = 'block';
            document.getElementById('chat').style.display = 'none';
        });

    document.getElementById('reg').style.display = 'none';
    document.getElementById('chat').style.display = 'block';
}

function disconnect() {
    if(stompClient != null) {
        stompClient.disconnect();
    }
    stompClient.send("/app/logout", {}, JSON.stringify({'userName': name, 'sessionId': sessionId}));
}

function isLongerThanBuf(text) {
    var isLonger = text.length >= 65536;
    if(isLonger){
        alert("Your message is too long :(");
    }

    return isLonger;
}

function sendMessage() {
    var text = textToSend.value;
    var isTextEmpty = isEmpty(text);
    textToSend.value = '';

    if(isTextEmpty || isLongerThanBuf(text)){
        return;
    }

    stompClient.send("/app/sendMessage", {},
        JSON.stringify({'userName':name, 'payload':text, 'sessionId': sessionId}));
}
