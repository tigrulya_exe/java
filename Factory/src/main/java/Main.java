import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.nsu.manasyan.factory.Factory;
import ru.nsu.manasyan.factory.view.MainController;
import ru.nsu.manasyan.factory.view.ViewLinker;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    private ViewLinker viewLinker;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("main.fxml"));
        stage.setScene(new Scene(loader.load()));
        stage.setResizable(false);

        MainController controller = loader.getController();
        viewLinker = new ViewLinker(controller,new Factory());

        stage.setOnHidden(e -> {
            viewLinker.stop();
//            Platform.exit();
        });

        stage.show();
    }

}
