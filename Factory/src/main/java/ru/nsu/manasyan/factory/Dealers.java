package ru.nsu.manasyan.factory;

import ru.nsu.manasyan.factory.carParts.Car;
import ru.nsu.manasyan.factory.util.PeriodicalExecutor;

public class Dealers extends PeriodicalExecutor {
    private static final int START_PERIOD_MS = 1400;

    private int dealersCount;

    public Dealers(int dealersCount){
        super(dealersCount, START_PERIOD_MS, dealersCount);
        this.dealersCount = dealersCount;
    }

    public void deal(Factory factory) {
        for (int i = 0; i < dealersCount; ++i) {
            final int dealerNum = i;
            execute(() -> {
                try {
                    Car car = factory.getCarStorage().get();
                    factory.addLog(car, dealerNum);
                } catch (InterruptedException e) {
//                    System.out.println("Speed was changed");
                }
            } );
        }
    }

}
