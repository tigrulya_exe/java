package ru.nsu.manasyan.factory;

import ru.nsu.manasyan.factory.assemblers.CarAssembler;
import ru.nsu.manasyan.factory.storages.CarPartStorage;
import ru.nsu.manasyan.factory.carParts.*;
import ru.nsu.manasyan.factory.storages.CarStorage;
import ru.nsu.manasyan.factory.suppliers.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Factory {
    private static final String PROPERTIES_PATH = "factory.properties";
    private boolean isLogMode;
    private Properties properties;

    private Map<UnitNum,Supplier> suppliers;
    private Map<UnitNum,CarPartStorage> carPartStorages;

    private CarAssembler carAssembler;
    private CarStorage carStorage;
    private Dealers dealers;
    
    private PrintWriter writer;

    public Factory() throws IOException{
        this.properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(PROPERTIES_PATH));
        this.isLogMode = Boolean.parseBoolean(properties.getProperty("logMode"));
        this.writer =  new PrintWriter(new FileWriter(properties.getProperty("logFileName"), true) );
        this.dealers = new Dealers(getIntProperty("dealersCount"));
        initSubUnits();
    }

    public void addLog(Car car, int dealerNum){
        if(!isLogMode){
            return;
        }
        synchronized (writer) {
            writer.println( new Date().toString() + " Dealer " + dealerNum +  car);
            writer.flush();
        }
    }

    public Supplier getSupplier(UnitNum unitNum){
        return suppliers.get(unitNum);
    }

    public CarPartStorage getCarPartStorage(UnitNum unitNum){
        return carPartStorages.get(unitNum);
    }

    public CarStorage getCarStorage() {
        return carStorage;
    }


    public void start(){
        carStorage.startController();
        carAssembler.initWorkersPool();
        suppliers.forEach((k,v)->v.supply());
        dealers.deal(this);
    }

    public void stop(){
        dealers.stop();
        carAssembler.stop();
        suppliers.forEach((k,v) -> v.stop());
        carPartStorages.forEach((k,v) -> v.stop());
        carStorage.stop();
    }

    public void close(){
        stop();
        suppliers.forEach((k,v) -> v.close());
        dealers.close();
    }

    public Dealers getDealers() {
        return dealers;
    }

    private void initCarPartStorages(){
        carPartStorages.put(UnitNum.BODY,new CarPartStorage<Body>(getIntProperty("storageBodySize")));

        carPartStorages.put(UnitNum.ENGINE, new CarPartStorage<Engine>(getIntProperty("storageEngineSize")));
        carPartStorages.put(UnitNum.ACCESSORY, new CarPartStorage<Accessory>(getIntProperty("storageAccessorySize")));
    }

    private void initSuppliers(){
        int defaultDelay = getIntProperty("defaultPeriod");

        suppliers.put(UnitNum.BODY,new CarPartSupplier<Body>(carPartStorages.get(UnitNum.BODY),defaultDelay, Body::new));
        suppliers.put(UnitNum.ENGINE,new CarPartSupplier<Engine>(carPartStorages.get(UnitNum.ENGINE),defaultDelay,Engine::new));
        suppliers.put(UnitNum.ACCESSORY,new CarPartSupplier<Accessory>(carPartStorages.get(UnitNum.ACCESSORY),defaultDelay,Accessory::new));
    }

    private void initSubUnits(){
        this.suppliers = new HashMap<>();
        this.carPartStorages = new HashMap<>();

        initCarPartStorages();
        initSuppliers();

        this.carAssembler = new CarAssembler(carPartStorages, getIntProperty("workersCount"));
        this.carStorage = new CarStorage(carAssembler, getIntProperty("storageCarSize"));
    }

    private int getIntProperty(String key){
        return Integer.parseInt(properties.getProperty(key));
    }

}
