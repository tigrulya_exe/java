package ru.nsu.manasyan.factory;

public enum UnitNum {
    BODY,
    ENGINE,
    ACCESSORY
}
