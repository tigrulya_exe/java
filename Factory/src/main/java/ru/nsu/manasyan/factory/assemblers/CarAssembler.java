package ru.nsu.manasyan.factory.assemblers;

import ru.nsu.manasyan.factory.storages.CarPartStorage;
import ru.nsu.manasyan.factory.carParts.*;
import ru.nsu.manasyan.factory.storages.CarStorage;
import ru.nsu.manasyan.factory.UnitNum;

import java.util.Map;
import java.util.concurrent.*;

public class CarAssembler {
    private CarPartStorage<Body> bodyStorage;
    private CarPartStorage<Engine> engineStorage;
    private CarPartStorage<Accessory> accessoryStorage;

    public void setCarStorage(CarStorage carStorage) {
        this.carStorage = carStorage;
    }

    private CarStorage carStorage;
    private ThreadPoolExecutor executorService;
    private int workersCount;

    public CarAssembler(CarPartStorage<Body> bodyStorage, CarPartStorage<Engine> engineStorage, CarPartStorage<Accessory> accessoryStorage, int workersCount) {

        this.bodyStorage = bodyStorage;
        this.engineStorage = engineStorage;
        this.accessoryStorage = accessoryStorage;
        this.workersCount = workersCount;
    }

    public CarAssembler(Map<UnitNum,CarPartStorage> carPartStorages, int workersCount) {
        this(carPartStorages.get(UnitNum.BODY),carPartStorages.get(UnitNum.ENGINE),carPartStorages.get(UnitNum.ACCESSORY), workersCount);
    }

    public void assembly(int count) {

        for (int i = 0; i < count; i++) {
            executorService.submit(() -> {
                try {
                    Body body = bodyStorage.get();
                    Engine engine = engineStorage.get();
                    Accessory accessory = accessoryStorage.get();

                    carStorage.put(new Car(engine, body, accessory));
                } catch (InterruptedException e) {
//                    e.printStackTrace();
                }
            });
        }
    }

    public void initWorkersPool()  {
        executorService = (ThreadPoolExecutor)Executors.newFixedThreadPool(workersCount);
        executorService.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
    }

    public  void stop(){
        if(executorService != null)
            executorService.shutdownNow();
    }
}
