package ru.nsu.manasyan.factory.carParts;

public class Accessory extends CarPart {
    private static int serialIds = 0;
    private static final String NAME = "ACCESSORY";


    public synchronized int incrementCount() {
        return ++serialIds;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
