package ru.nsu.manasyan.factory.carParts;

public class Body extends CarPart {
    private static int serialIds = 0;
    private static final String NAME = "BODY";

    public synchronized int incrementCount() {
        return ++serialIds;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
