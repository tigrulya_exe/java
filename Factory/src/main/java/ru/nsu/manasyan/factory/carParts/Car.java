package ru.nsu.manasyan.factory.carParts;

public class Car {
    private static int serialIds = 0;
    private int serialId;
    private Engine engine;
    private Body body;
    private Accessory accessory;


    public synchronized int incrementCount() {
        return ++serialIds;
    }

    public int getSerialId() {
        return serialId;
    }

    public Engine getEngine() {
        return engine;
    }

    public Body getBody() {
        return body;
    }

    public Accessory getAccessory() {
        return accessory;
    }

    public Car(Engine engine, Body body, Accessory accessory) {
        this.serialId = incrementCount();
        this.engine = engine;
        this.body = body;
        this.accessory = accessory;
    }

    @Override
    public String toString(){
        return "{" + "Car " + getSerialId() + " " + engine + " " + body + " " + accessory + "}";
    }
}
