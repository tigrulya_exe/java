package ru.nsu.manasyan.factory.carParts;

public abstract class CarPart implements ICarPart {
    private final int serialId;

    @Override
    public int getSerialId() {
        return serialId;
    }

    public CarPart() {
        this.serialId = incrementCount();
    }

    @Override
    public String toString() {
        return getName() + ": " + getSerialId();
    }
}
