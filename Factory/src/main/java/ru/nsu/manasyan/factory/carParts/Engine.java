package ru.nsu.manasyan.factory.carParts;

public class Engine extends CarPart {
    private static int serialIds = 0;
    private static final String NAME = "ENGINE";

    public synchronized int incrementCount() {
        return ++serialIds;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
