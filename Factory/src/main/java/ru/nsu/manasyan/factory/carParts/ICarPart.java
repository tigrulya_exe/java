package ru.nsu.manasyan.factory.carParts;

public interface ICarPart {
    int getSerialId();
    int incrementCount();
    String getName();
}
