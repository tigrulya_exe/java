package ru.nsu.manasyan.factory.storages;

import ru.nsu.manasyan.factory.carParts.ICarPart;
import ru.nsu.manasyan.factory.util.ObservableCountValue;
import ru.nsu.manasyan.factory.util.CountValueListener;

import java.util.ArrayDeque;
import java.util.Deque;

public class CarPartStorage<E extends ICarPart>  implements ICarPartStorage<E>{
    private static final int COUNT_START_VALUE = 0;

    private final int maxPartsCount;
    private Deque<E> deque;
    private ObservableCountValue partCount;

    public CarPartStorage(int maxPartsCount) {
        this.maxPartsCount = maxPartsCount;
        this.deque = new ArrayDeque<>();
        this.partCount = new ObservableCountValue();
    }

    @Override
    public synchronized void put(E carPart) throws InterruptedException {
        while(partCount.getValue() == maxPartsCount){
            wait();
        }

        deque.add(carPart);
        partCount.increment();
        notify();
    }

    public void registerListeners(CountValueListener ... listeners){
        for(var listener : listeners){
            partCount.registerListener(listener);
        }
    }

    @Override
    public synchronized E get() throws InterruptedException {
        while(partCount.getValue() == 0){
            wait();
        }
        partCount.decrement();
        notifyAll();
        return deque.pop();
    }

    @Override
    public void stop(){
        partCount.setValue(COUNT_START_VALUE);
        deque.clear();
    }
}
