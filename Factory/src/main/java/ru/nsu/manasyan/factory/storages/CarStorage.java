package ru.nsu.manasyan.factory.storages;

import ru.nsu.manasyan.factory.assemblers.CarAssembler;
import ru.nsu.manasyan.factory.carParts.Car;
import ru.nsu.manasyan.factory.util.ObservableCountValue;
import ru.nsu.manasyan.factory.util.CountValueListener;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.atomic.AtomicInteger;

public class CarStorage {
    private static final int COUNT_START_VALUE = 0;

    private final int maxCarsCount;
    private Deque<Car> deque;
    private ObservableCountValue carCount;
    private ObservableCountValue totalCount;

    private int carInCount;

    private Thread controllerThread;
    private Runnable controllerTask;


    public CarStorage(CarAssembler carAssembler, int maxCarsCount){
        carAssembler.setCarStorage(this);
        this.maxCarsCount = maxCarsCount;
        this.deque = new ArrayDeque<>();
        this.carCount = new ObservableCountValue();
        this.totalCount = new ObservableCountValue();
        this.controllerTask = new CarStorageController(carAssembler, this);
    }

    int getMaxCarsCount() {
        return maxCarsCount;
    }

    int getCurrCarCount(){
        return deque.size();
    }

    public synchronized void put(Car car) throws InterruptedException {
        while (deque.size() == maxCarsCount){
            wait();
        }

        deque.add(car);
        carCount.increment();
        ++carInCount;
        notify();
    }

    public synchronized Car get() throws InterruptedException {
        while (deque.isEmpty()){
            wait();
        }

        carCount.decrement();
        totalCount.increment();
        notifyAll();
        return deque.pop();
    }


    public void registerListeners(CountValueListener... listeners){
        for(var listener : listeners){
            carCount.registerListener(listener);
        }
    }

    public ObservableCountValue getTotalCount() {
        return totalCount;
    }

    public void stop() {
        carCount.setValue(COUNT_START_VALUE);
        totalCount.setValue(COUNT_START_VALUE);
        deque.clear();
        if(controllerThread != null)
            controllerThread.interrupt();
    }

    public void startController(){
        this.controllerThread =  new Thread(controllerTask);
        this.controllerThread.start();
    }

    public int getCarInCount() {
        return carInCount;
    }
}
