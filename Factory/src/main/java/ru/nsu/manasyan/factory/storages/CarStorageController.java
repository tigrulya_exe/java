package ru.nsu.manasyan.factory.storages;

import ru.nsu.manasyan.factory.assemblers.CarAssembler;

public class CarStorageController implements Runnable{

    private CarAssembler carAssembler;
    private final CarStorage carStorage;
    private int assemblyCount;

    CarStorageController(CarAssembler carAssembler, CarStorage carStorage){
        this.carAssembler = carAssembler;
        this.carStorage = carStorage;
    }

    @Override
    public void run() {
        int maxSize = carStorage.getMaxCarsCount();
        int lastInSize, currInSize;

        synchronized (carStorage){
            try {
                while (true) {
                    currInSize = carStorage.getCarInCount();
                    assemblyCount = maxSize - carStorage.getCurrCarCount();

                    lastInSize = currInSize;
                    carAssembler.assembly(assemblyCount);

                    while (carStorage.getCarInCount() - lastInSize != assemblyCount){
                        carStorage.wait();
                    }
                }
            }catch (InterruptedException e) {
                System.out.println("Controller was interrupted");
            }
        }
    }
}
