package ru.nsu.manasyan.factory.storages;

import ru.nsu.manasyan.factory.carParts.ICarPart;

public interface ICarPartStorage<E extends ICarPart> {
    void put(E carPart) throws InterruptedException;
    E get() throws InterruptedException;
    void stop();
}
