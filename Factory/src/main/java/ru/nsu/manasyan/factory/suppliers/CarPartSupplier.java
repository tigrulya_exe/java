package ru.nsu.manasyan.factory.suppliers;

import ru.nsu.manasyan.factory.storages.CarPartStorage;
import ru.nsu.manasyan.factory.carParts.ICarPart;
import ru.nsu.manasyan.factory.util.ObservableCountValue;
import ru.nsu.manasyan.factory.util.CountValueListener;
import ru.nsu.manasyan.factory.util.PeriodicalExecutor;


public class CarPartSupplier<E extends ICarPart> extends PeriodicalExecutor implements Supplier {

    private static final int DEFAULT_SUPPLIERS_COUNT = 1;
    private static final int COUNT_START_VALUE = 0;
    private static final String ERROR_MESSAGE = "Supply interrupted ";

    private ObservableCountValue partsSupplied;
    private CarPartStorage<E> storage;
    private java.util.function.Supplier<E> carPartCreator;
    private int suppliersCount;

    public CarPartSupplier(CarPartStorage<E> storage, int periodMs, java.util.function.Supplier<E> carPartCreator){
        super(DEFAULT_SUPPLIERS_COUNT,periodMs);
        this.storage = storage;
        this.carPartCreator = carPartCreator;
        this.partsSupplied = new ObservableCountValue();
        this.suppliersCount = DEFAULT_SUPPLIERS_COUNT;
    }

    @Override
    public void supply() {
        for (int i = 0; i < suppliersCount; ++i) {
            execute(() -> {
                try {
                    storage.put(carPartCreator.get());
                    partsSupplied.increment();
                } catch (InterruptedException e) {
                    System.out.println(ERROR_MESSAGE + Thread.currentThread().getName());
                }
            });
        }
    }

    @Override
    public void registerListeners(CountValueListener... listeners) {
        for(var listener : listeners){
            partsSupplied.registerListener(listener);
        }
    }

    @Override
    public void setSuppliersCount(int count){
        this.suppliersCount = count;
    }

    @Override
    public synchronized void stop(){
        partsSupplied.setValue(COUNT_START_VALUE);
        super.stop();
    }

 }

