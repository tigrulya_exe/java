package ru.nsu.manasyan.factory.suppliers;

import ru.nsu.manasyan.factory.util.CountValueListener;

public interface Supplier<E> {

    void supply();
    void setPeriodMs(int speedConst);
    void registerListeners(CountValueListener... listeners);
    void setSuppliersCount(int count);
    void stop();
    void close();
}
