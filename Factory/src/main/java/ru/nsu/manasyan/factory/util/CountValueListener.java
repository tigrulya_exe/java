package ru.nsu.manasyan.factory.util;

@FunctionalInterface
public interface CountValueListener {
    void countValueChanged(int newValue);
}
