package ru.nsu.manasyan.factory.util;

import java.util.ArrayList;
import java.util.List;

public class ObservableCountValue {
    private int value;
    private List<CountValueListener> listeners;

    public ObservableCountValue(){
        this.value = 0;
        this.listeners = new ArrayList<>();
    }

    public void increment(){
        ++value;
        notifyAllListeners();
    }

    public void decrement(){
        --value;
        notifyAllListeners();
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
            return value;
    }

    public void registerListener(CountValueListener countValueListener){
            listeners.add(countValueListener);
    }

    private void notifyAllListeners(){
        for(var listener : listeners){
            listener.countValueChanged(value);
        }
    }
}
