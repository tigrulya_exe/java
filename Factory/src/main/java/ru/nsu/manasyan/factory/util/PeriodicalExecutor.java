package ru.nsu.manasyan.factory.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PeriodicalExecutor {
    private static final int DEFAULT_DELAY = 0;
    private static final int DEFAULT_EXECUTORS_COUNT = 1;

    private List<ScheduledFuture<?>> handles;
    private ScheduledThreadPoolExecutor executorService;

    private int delayMs;
    private int periodMs;
    private int executorsCount;

    private Runnable lastTask;

    public PeriodicalExecutor(int threadCount, int periodMs, int executorsCount){
        this.executorService = new ScheduledThreadPoolExecutor(threadCount);
        this.periodMs = periodMs;
        this.delayMs = DEFAULT_DELAY;
        this.executorsCount = executorsCount;
        this.handles = new ArrayList<>();
    }

    public PeriodicalExecutor(int threadCount, int periodMs){
        this(threadCount, periodMs, DEFAULT_EXECUTORS_COUNT);
    }

    public void setPeriodMs(int periodMs) {
        this.periodMs = periodMs;

        for(int i = 0 ; i < executorsCount; ++i){
            handles.get(i).cancel(true);
            handles.set(i, getHandle());
        }
    }

    public void stop(){
        if(handles.size() != 0){
            handles.forEach((v)->v.cancel(true));
        }
        handles.clear();
    }

    public void close(){
        stop();
        executorService.shutdownNow();
    }

    protected void execute(Runnable task){
        lastTask = task;
        handles.add(getHandle());
    }

    private ScheduledFuture<?> getHandle (){
        return executorService.scheduleAtFixedRate(lastTask, delayMs, periodMs, TimeUnit.MILLISECONDS);
    }
}
