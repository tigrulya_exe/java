package ru.nsu.manasyan.factory.view;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import ru.nsu.manasyan.factory.UnitNum;
import ru.nsu.manasyan.factory.Factory;

public class MainController {
    private static final int DEFAULT_PERIOD_STEP_MS = 150;
    private static final int DEFAULT_PERIOD_START_MS = 700;

    private Factory factory;

    @FXML
    private TextField totalCarsCount;

    @FXML
    private TextField storageCarCount;

    @FXML
    private TextField storageBodyCount;

    @FXML
    private TextField storageEngineCount;

    @FXML
    private TextField storageAccessoryCount;

    @FXML
    private TextField suppliedBodiesCount;

    @FXML
    private TextField suppliedEnginesCount;

    @FXML
    private TextField suppliedAccessoriesCount;

    @FXML
    private Slider bodySupplySpeed;

    @FXML
    private Slider engineSupplySpeed;

    @FXML
    private Slider accessorySupplySpeed;

    @FXML
    private Slider dealersSpeed;

    @FXML
    private Button startButton;

    @FXML
    private Button stopButton;

    public void setFactory(Factory factory) {
        this.factory = factory;
    }

    @FXML
    public void initialize(){
        bodySupplySpeed.valueProperty().addListener((Obs, oldV, newV)-> sliderSupplierValueChanged(newV.intValue(), UnitNum.BODY));
        engineSupplySpeed.valueProperty().addListener((Obs, oldV, newV)-> sliderSupplierValueChanged(newV.intValue(), UnitNum.ENGINE));
        accessorySupplySpeed.valueProperty().addListener((Obs, oldV, newV)-> sliderSupplierValueChanged(newV.intValue(), UnitNum.ACCESSORY));
        dealersSpeed.valueProperty().addListener((Obs,oldV, newV) -> sliderDealerValueChanged(newV.intValue() ));
    }

    private void sliderSupplierValueChanged(int value, UnitNum unitNum){
        int period = DEFAULT_PERIOD_START_MS - DEFAULT_PERIOD_STEP_MS * (value - 1);
        factory.getSupplier(unitNum).setPeriodMs((period));
    }

    private void sliderDealerValueChanged(int value){
        int period = 2 * DEFAULT_PERIOD_START_MS - DEFAULT_PERIOD_STEP_MS * (value - 1);
        factory.getDealers().setPeriodMs((period));
    }

    private void updateTextField(TextField textField, String newValue){
        Platform.runLater(()->textField.setText(newValue));
    }

    @FXML
    public void updateStorageBodyCount(int newValue){
        updateTextField(storageBodyCount,String.valueOf(newValue));
    }

    @FXML
    public void updateStorageEngineCount(int newValue){
        updateTextField(storageEngineCount,String.valueOf(newValue));
    }

    @FXML
    public void updateStorageAccessoryCount(int newValue){
        updateTextField(storageAccessoryCount,String.valueOf(newValue));
    }

    @FXML
    public void updateSuppliedBodiesCount(int newValue){
        updateTextField(suppliedBodiesCount,String.valueOf(newValue));
    }

    @FXML
    public void updateSuppliedEnginesCount(int newValue){
        updateTextField(suppliedEnginesCount,String.valueOf(newValue));
    }

    @FXML
    public void updateSuppliedAccessoriesCount(int newValue){
        updateTextField(suppliedAccessoriesCount,String.valueOf(newValue));
    }

    @FXML
    public void updateStorageCarCount(int newValue){
        updateTextField(storageCarCount, String.valueOf(newValue));
    }

    @FXML
    public void updateTotalCarsCount(int newValue){
        updateTextField(totalCarsCount,String.valueOf(newValue));
    }

    @FXML
    public void start(){
        setSlidersDisability(false);
        startButton.setDisable(true);
        stopButton.setDisable(false);
        factory.start();
    }

    @FXML
    public void stop(){
        setSlidersDisability(true);
        stopButton.setDisable(true);
        factory.stop();
        startButton.setDisable(false);
    }

    private void setSlidersDisability(boolean isDisable){
        dealersSpeed.setDisable(isDisable);
        engineSupplySpeed.setDisable(isDisable);
        accessorySupplySpeed.setDisable(isDisable);
        bodySupplySpeed.setDisable(isDisable);
        dealersSpeed.setDisable(isDisable);
    }
}
