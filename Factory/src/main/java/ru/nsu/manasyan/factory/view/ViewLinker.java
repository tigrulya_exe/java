package ru.nsu.manasyan.factory.view;

import ru.nsu.manasyan.factory.Factory;
import ru.nsu.manasyan.factory.UnitNum;


public class ViewLinker {
    private Factory factory;

    private void initStorageListeners(MainController controller){
        factory.getCarPartStorage(UnitNum.BODY).registerListeners(controller::updateStorageBodyCount);
        factory.getCarPartStorage(UnitNum.ENGINE).registerListeners(controller::updateStorageEngineCount);
        factory.getCarPartStorage(UnitNum.ACCESSORY).registerListeners(controller::updateStorageAccessoryCount);
        factory.getCarStorage().registerListeners(controller::updateStorageCarCount);
        factory.getCarStorage().getTotalCount().registerListener(controller::updateTotalCarsCount);
    }

    //TODO suppliers and storages should implement ObservableUnit interface
    private void initSupplierListeners(MainController controller){
        factory.getSupplier(UnitNum.BODY).registerListeners(controller::updateSuppliedBodiesCount);
        factory.getSupplier(UnitNum.ENGINE).registerListeners(controller::updateSuppliedEnginesCount);
        factory.getSupplier(UnitNum.ACCESSORY).registerListeners(controller::updateSuppliedAccessoriesCount);
    }

    public ViewLinker(MainController controller, Factory factory){
        this.factory = factory;
        controller.setFactory(factory);
        initStorageListeners(controller);
        initSupplierListeners(controller);
    }

    public void stop(){
        factory.close();
    }
}
